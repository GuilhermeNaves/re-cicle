//
//  Inicial.m
//  PriceCar
//
//  Created by Guilherme Naves on 3/24/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import "Inicial.h"

@implementation Inicial

+(id)TrashWithType:(NSString *)type {
    return [[Inicial alloc] initWithType:type];
}
static Inicial *inici = nil;

-(id)initWithType:(NSString *)type {
    self= [super init];
    if (self) {
        self.typeTrash = type;
    }
    return self;
}

- (NSString *) descr {
    return [NSString stringWithFormat:@"%@", self.typeTrash];
}

@end
