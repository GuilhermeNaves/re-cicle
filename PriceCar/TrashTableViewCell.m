//
//  TrashTableViewCell.m
//  PriceCar
//
//  Created by Guilherme Naves on 3/25/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import "TrashTableViewCell.h"
#import "TrashTableViewController.h"
#import "Trash.h"
#import "TrashSelector.h"
@implementation TrashTableViewCell {
NSMutableArray *_trashes;
}

+(id)sharedInstance {
    
    static TrashSelector *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[TrashSelector alloc] init];
    });
    return _sharedInstance;
}

-(instancetype)init {
    self = [super init];
    if (self) {
        _trashes = [[NSMutableArray alloc] init];
        [self pop];
    }
    return self;
}

- (int)count {
    return (int)_trashes.count;
}

- (NSArray *)trashes {
    return _trashes;
}

- (void)pop {
    NSError *error;
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *trashSelecionado = _trashSelecionado.nomeTrash;
    
    NSString *path = [bundle pathForResource: trashSelecionado ofType:@"txt"];
    NSLog(@"%@",path);
    NSString *trashDesc = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    NSArray *trashApp = [trashDesc componentsSeparatedByString:@"\n"];
    
    _trashes = [[NSMutableArray alloc] init];
    
    for (int i=0; i<trashApp.count; i++) {
        
        Trash *t = [[Trash alloc] init];
        t.trashType = [[[trashApp objectAtIndex:i] componentsSeparatedByString:@"|"] objectAtIndex:0];
        t.action = [[[trashApp objectAtIndex:i] componentsSeparatedByString:@"|"] objectAtIndex:1];
        t.image = [[[trashApp objectAtIndex:i] componentsSeparatedByString:@"|"] objectAtIndex:2];
        
        
        [_trashes addObject:t];
    }
}
@end