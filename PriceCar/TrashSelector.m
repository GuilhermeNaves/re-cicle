//
//  TrashSelector.m
//  PriceCar
//
//  Created by Guilherme Naves on 3/26/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import "TrashSelector.h"
#import "Trash.h"

@implementation TrashSelector {
    NSMutableArray *_trashes;
    NSArray *_az;
}

+(id)sharedInstance {
    
    static TrashSelector *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[TrashSelector alloc] init];
        });
    return _sharedInstance;
}

//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//        _trashes = [[NSMutableArray alloc]init];
//        _az = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
//        [self pop]
//    }
//    return self;
//}
//
//-(instancetype)init {
//    self = [super init];
//    if (self) {
//        _trashes = [[NSMutableArray alloc] init];
////        [self pop];
//    }
//    return self;
//}

- (int)count {
    return (int)_trashes.count;
}

- (NSArray *)trashes {
    return _trashes;
}

- (void)pop:(NSString *)trashSelecionado {
    NSError *error;
    NSBundle *bundle = [NSBundle mainBundle];
    
    NSString *path = [bundle pathForResource: trashSelecionado ofType:@"txt"];
    NSLog(@"%@",path);
    NSString *trashDesc = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    NSArray *trashApp = [trashDesc componentsSeparatedByString:@"\n"];
    
    _trashes = [[NSMutableArray alloc] init];
    
    for (int i=0; i<trashApp.count; i++) {
        
        Trash *t = [[Trash alloc] init];
        t.trashType = [[[trashApp objectAtIndex:i] componentsSeparatedByString:@"|"] objectAtIndex:0];
        t.action = [[[trashApp objectAtIndex:i] componentsSeparatedByString:@"|"] objectAtIndex:1];
        t.image = [[[trashApp objectAtIndex:i] componentsSeparatedByString:@"|"] objectAtIndex:2];
        
        
        [_trashes addObject:t];
    }
}
@end
