//
//  Inicial.h
//  PriceCar
//
//  Created by Guilherme Naves on 3/24/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Inicial : NSObject

@property NSString *typeTrash;

+(id)TrashWithType:(NSString *)type;
-(id)initWithType:(NSString *)type;

@end
