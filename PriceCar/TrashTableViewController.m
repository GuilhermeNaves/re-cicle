//
//  TrashTableViewController.m
//  PriceCar
//
//  Created by Guilherme Naves on 3/25/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import "TrashTableViewController.h"
#import "Inicial.h"
#import "ViewController.h"
#import "TrashSelector.h"
#import "TrashTableViewCell.h"
#import "Trash.h"

@interface TrashTableViewController (){
    BOOL _searchAtivo;
    NSMutableDictionary *_lixoFiltrado;
    NSMutableArray *categorias;
    NSArray *_secaoFiltrar;
}

@property (weak, nonatomic) IBOutlet UILabel *nomeCat;

@end

@implementation TrashTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _searchAtivo = NO;
    _lixoFiltrado = [[NSMutableDictionary alloc] init];
    _nomeCat.text = _catselect.typeTrash;
    
    TrashSelector  *ts = [[TrashSelector sharedInstance] init];
    [ts pop:_catselect.typeTrash];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[TrashSelector sharedInstance] trashes] count];
}

-(UITableViewCell *)tableView: (UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TrashTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    Trash *trash = [[[TrashSelector sharedInstance] trashes] objectAtIndex:indexPath.row];
    cell.trashType.text = trash.trashType;
    cell.image.image = [UIImage imageNamed:trash.image];
    cell.action.image = [UIImage imageNamed:trash.action];
    return cell;
}



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
