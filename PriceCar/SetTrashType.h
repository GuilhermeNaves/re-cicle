//
//  SetTrashType.h
//  PriceCar
//
//  Created by Guilherme Naves on 3/24/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SetTrashType : NSObject

+(id)sharedInstance;
-(int)count;
-(NSArray *)trashes;

@end
