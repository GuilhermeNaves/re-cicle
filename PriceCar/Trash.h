//
//  Trash.h
//  PriceCar
//
//  Created by Guilherme Naves on 3/26/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Trash : NSObject

@property NSString *nomeTrash;
@property NSString *image;
@property NSString *trashType;
@property NSString *action;

+ (id)TrashWithTrash:(NSString *)trashname;
- (id)initWithTrash:(NSString *)trashname;

@end
