//
//  TrashSelector.h
//  PriceCar
//
//  Created by Guilherme Naves on 3/26/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Trash;
@interface TrashSelector : NSObject

+ (id)sharedInstance;

- (int)count;
- (NSArray *)trashes;
@property Trash *trashSelecionado;
- (void)pop:(NSString *)trashSelecionado;

@end
