//
//  ViewController.m
//  PriceCar
//
//  Created by Guilherme Naves on 3/24/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import "ViewController.h"
#import "Inicial.h"
#import "SetTrashType.h"
#import "TrashTableViewController.h"

@interface ViewController (){
    Inicial *picker;
}

@property (weak, nonatomic) IBOutlet UIButton *entrar;
@end

@implementation ViewController{
    SetTrashType *stt;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    stt = [[SetTrashType sharedInstance]init];
    _entrar.hidden = YES;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [stt count];
    
}

- (NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSArray *trashes = [stt trashes];
    Inicial *trash = [trashes objectAtIndex:row];
    
    return trash.typeTrash;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSArray *categos = [stt trashes];
    Inicial *cat1 = [categos objectAtIndex:row];
    picker = [categos objectAtIndex:row];
    NSLog(@"%@", cat1.typeTrash);
    _entrar.hidden=NO;
}

- (IBAction)goToTable:(id)sender {
    
   TrashTableViewController  *trash = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"goToTable"];
    
    trash.catselect = picker;
    
    [self presentViewController:trash animated:YES completion:nil];
    
}


@end
