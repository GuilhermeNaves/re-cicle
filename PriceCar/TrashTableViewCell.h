//
//  TrashTableViewCell.h
//  PriceCar
//
//  Created by Guilherme Naves on 3/25/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TrashTableViewController;
@class Trash;
@interface TrashTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *trashType;
@property (weak, nonatomic) IBOutlet UIImageView *action;
+ (id)sharedInstance;

- (int)count;
- (NSArray *)trashes;
@property Trash *trashSelecionado;
@end

