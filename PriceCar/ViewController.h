//
//  ViewController.h
//  PriceCar
//
//  Created by Guilherme Naves on 3/24/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>
- (IBAction)goToTable:(id)sender;

@property NSArray *categos;

@end

