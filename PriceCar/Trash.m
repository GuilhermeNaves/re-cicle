//
//  Trash.m
//  PriceCar
//
//  Created by Guilherme Naves on 3/26/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import "Trash.h"

@implementation Trash

+ (id)TrashWithTrash:(NSString *)trashname {
    Trash *tra = [[Trash alloc] initWithTrash:trashname];
    return tra;
}

- (id)initWithTrash:(NSString *)trashname {
    self = [super init];
    if (self) {
        _trashType = trashname;
    }
    return self;
}

-(NSString *)desc {
    return [NSString stringWithFormat:@"%@", self.nomeTrash];
}

@end
