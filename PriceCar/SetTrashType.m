
//
//  SetTrashType.m
//  PriceCar
//
//  Created by Guilherme Naves on 3/24/15.
//  Copyright (c) 2015 Mpex. All rights reserved.
//

#import "SetTrashType.h"
#import "Inicial.h"

@implementation SetTrashType {
    NSMutableArray *_trashes;
}

+ (id)sharedInstance{
    
    static SetTrashType *_sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        _sharedInstance = [[SetTrashType alloc] init];
    });
    
    return  _sharedInstance;
    
}

- (instancetype)init{
    self = [super init];
    if (self) {
        _trashes = [[NSMutableArray alloc] init];
        [self pop];
    }
    return self;
}

- (int)count {
    return (int)_trashes.count;
}

- (NSArray *)trashes{
    return _trashes;
}

- (void)pop;
{
    Inicial *t[4];
    
    t[0] = [Inicial TrashWithType:@"METAL"];
    
    t[1] = [Inicial TrashWithType:@"PAPEL"];
    
    t[2] = [Inicial TrashWithType:@"PLÁSTICO"];
    
    t[3] = [Inicial TrashWithType:@"VIDRO"];
    
    for (int i=0; i<4; i++) {
        [_trashes addObject:t[i]];
    }
    
}

@end
